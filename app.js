const greet = (name) => {
    console.log(`hello ${name}`);
}

const welcome = (name) => {
    console.log(`Welcome ${name}`);
}

console.log('Welcome to the new app');

console.log('Thanks for using the app');
console.log('Random changes');

module.exports = { greet, welcome }