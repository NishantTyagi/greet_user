# [1.8.0](https://gitlab.com/NishantTyagi/greet_user/compare/v1.7.0...v1.8.0) (2020-11-17)


### Features

* changes to config file ([ba8ff80](https://gitlab.com/NishantTyagi/greet_user/commit/ba8ff8051bedebdfc32e2aa31be8ac0f0d959625))
* changes to the config file ([31e74ae](https://gitlab.com/NishantTyagi/greet_user/commit/31e74ae3dedc6ea29942150de0d44556bebee765))
* new features added in app.js ([b7a285f](https://gitlab.com/NishantTyagi/greet_user/commit/b7a285f89fb41bd56462ee598fcfef9841e1e1f7))

# [1.7.0](https://gitlab.com/NishantTyagi/greet_user/compare/v1.6.0...v1.7.0) (2020-11-17)


### Features

* big changes made to project ([4a66a42](https://gitlab.com/NishantTyagi/greet_user/commit/4a66a42fc6ae444c286fc2dfe0f6ada018dff464))

# [1.4.0](https://gitlab.com/NishantTyagi/greet_user/compare/v1.3.1...v1.4.0) (2020-11-13)


### Features

* changes to package.json and app.js ([cc27158](https://gitlab.com/NishantTyagi/greet_user/commit/cc2715869e441f8cf75cb365f2106f992785cddd))

## [1.3.1](https://gitlab.com/NishantTyagi/greet_user/compare/v1.3.0...v1.3.1) (2020-11-13)


### Bug Fixes

* fixed CI configuration ([a2a7367](https://gitlab.com/NishantTyagi/greet_user/commit/a2a736755ab9913da677c5975e2b834cd3e16310))

# [1.3.0](https://gitlab.com/NishantTyagi/greet_user/compare/v1.2.0...v1.3.0) (2020-11-13)


### Features

* new function added for welcome ([33d1efa](https://gitlab.com/NishantTyagi/greet_user/commit/33d1efabf655863bccf8dbe9235a7eb7901341b0))

# [1.2.0](https://gitlab.com/NishantTyagi/greet_user/compare/v1.1.6...v1.2.0) (2020-11-13)


### Features

* new features added for logging ([1d7d66e](https://gitlab.com/NishantTyagi/greet_user/commit/1d7d66e90ca65942e852dde2ad2f00586a98e4d8))

## [1.1.6](https://gitlab.com/NishantTyagi/greet_user/compare/v1.1.5...v1.1.6) (2020-11-13)


### Bug Fixes

* version.txt added ([df957b4](https://gitlab.com/NishantTyagi/greet_user/commit/df957b4248066d4bad17cacd3f71b52eb836abcf))

## [1.1.5](https://gitlab.com/NishantTyagi/greet_user/compare/v1.1.4...v1.1.5) (2020-11-12)


### Bug Fixes

* changes made to config file ([4becd2e](https://gitlab.com/NishantTyagi/greet_user/commit/4becd2e9f9b7c394e6c6f4a8953fdab628b5b2dc))
* semantic release file changed ([c37b268](https://gitlab.com/NishantTyagi/greet_user/commit/c37b26829849f1feb26f3399193e485fe19a91f2))
* something changed ([5b3338c](https://gitlab.com/NishantTyagi/greet_user/commit/5b3338c3e11036ea5b48f76212dde64633c57b42))
* something changed 2 ([3882ffd](https://gitlab.com/NishantTyagi/greet_user/commit/3882ffd0c84df0cb3dc1d394fa4ee421e732043d))
* something changed 3 ([0da5cf6](https://gitlab.com/NishantTyagi/greet_user/commit/0da5cf65be51b06b33c3fd4d02cd3b24fa794d2b))

## [1.1.4](https://gitlab.com/NishantTyagi/greet_user/compare/v1.1.3...v1.1.4) (2020-11-12)


### Bug Fixes

* config file manipulated ([fbbbd1f](https://gitlab.com/NishantTyagi/greet_user/commit/fbbbd1fe515bd5b09935b88a71a27e0a8c37ab23))
* semantic release config changed ([8b30ad1](https://gitlab.com/NishantTyagi/greet_user/commit/8b30ad1cc8eb6f7bc6830bfc3ca869aa207fe7e9))

## [1.1.3](https://gitlab.com/NishantTyagi/greet_user/compare/v1.1.2...v1.1.3) (2020-11-12)


### Bug Fixes

* little bit tweaks to the code ([734c1ed](https://gitlab.com/NishantTyagi/greet_user/commit/734c1edd2ee1faaa10366371f651739ee682185d))
* some package changes ([f9f5d16](https://gitlab.com/NishantTyagi/greet_user/commit/f9f5d1607aeb9d8817490572fef878ae3fbe130c))
* some package installed ([706ba0e](https://gitlab.com/NishantTyagi/greet_user/commit/706ba0e1d949b571a3cb35d819438e73b7854b55))
